# Mars Global Cluster!
High Performance local I/O storage with Indestructable Archival & Backup!s
- Local is RAID
- Global is Erasure Code!

Local is replicated to a Global Erasure Code Pool!
This allows very high performance locally, and yet almost Indestructable archive storage!
Offers best of both worlds.

## Topology
from: Local - LVM, OpenEBS, Btrfs raid on MARS
-> (network, wan or internet) ->
to: Global Unistor - erasure coded pool!

These Erasure Code pools are built on:
- Gluster
- Ceph
- Sheepdog

## Features:
This combines High-performance local store and backups to an efficient erasure coded pool. :-)
- Have high performance I/O from Local Raid!
- Minimum backup overhead by erasure coding at Global Scale.
- Can store Cold Data indefinitely on Tape, or a Crypto storage coin! like: Sia, StorJ, Filecoin
